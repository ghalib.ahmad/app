# Demo app

## Usage
The easiest way to run the demo app is to use  Docker compose as follows:
```
docker-compose up
```
Then open your browser to http://127.0.0.1:5000
## Developer environment
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python app.py
```
