#!/usr/bin/env python3
from flask import Flask
from flask import render_template
from flask import request
import os
import random
import requests
import socket
import sys

from unsplash.api import Api
from unsplash.auth import Auth

app = Flask(__name__)


client_id = os.getenv('API_CLIENT_ID')
client_secret = os.getenv('API_CLIENT_SECRET')
redirect_uri = "urn:ietf:wg:oauth:2.0:oob"

if not client_id and not client_secret:
    print('Env vars API_CLIENT_ID and API_CLIENT_SECRET are required')
    sys.exit(1)


auth = Auth(client_id, client_secret, redirect_uri)
api = Api(auth)

@app.route('/health')
def health():
    return ''


@app.route('/')
def index():
    templates = {'text/html': 'index.html', 'text/plain': 'index.txt'}
    mime = request.accept_mimetypes.best_match(['text/plain', 'text/html'],
                                               'text/plain')

    # stockholm2-<rsis>-<podid>
    city = socket.gethostname().split('-', 1)[0].translate('0123456789')
    try:
        image = random.choice(api.search.photos(city)['results'])
    except:
        image = None
    return render_template(templates[mime], hostname=socket.gethostname(),
                           image=image)


if __name__ == '__main__':
    app.run(debug=False, port=5000)
