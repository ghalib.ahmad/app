FROM python:3-alpine
WORKDIR /app
ADD requirements.txt .
RUN pip install -r requirements.txt
ADD . .
EXPOSE 5000
ENTRYPOINT ["gunicorn", "app:app", "-b", "0.0.0.0:5000"]
